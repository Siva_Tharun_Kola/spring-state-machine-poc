package com.example.springstatemachine;

import java.util.List;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import lombok.Data;

@Data
@Component
@ConfigurationProperties(prefix = "task")
public class TaskStateMachineProps {
  public Integer getStateMachinePoolSize() {
    return stateMachinePoolSize;
  }

  public void setStateMachinePoolSize(Integer stateMachinePoolSize) {
    this.stateMachinePoolSize = stateMachinePoolSize;
  }

  public List<TaskStatutes> getDailyTaskStates() {
    return dailyTaskStates;
  }

  public void setDailyTaskStates(List<TaskStatutes> dailyTaskStates) {
    this.dailyTaskStates = dailyTaskStates;
  }

  private Integer stateMachinePoolSize;

  private List<TaskStatutes> dailyTaskStates;

  @Data
  public static class TaskStatutes {
    private String                state;
    private List<TaskStateEvents> stateEvents;

    public String getState() {
      return state;
    }

    public void setState(String state) {
      this.state = state;
    }

    public List<TaskStateEvents> getStateEvents() {
      return stateEvents;
    }

    public void setStateEvents(List<TaskStateEvents> stateEvents) {
      this.stateEvents = stateEvents;
    }

    @Data
    public static class TaskStateEvents {
      private String nextState;
      private String triggerEvent;

      public String getNextState() {
        return nextState;
      }

      public void setNextState(String nextState) {
        this.nextState = nextState;
      }

      public String getTriggerEvent() {
        return triggerEvent;
      }

      public void setTriggerEvent(String triggerEvent) {
        this.triggerEvent = triggerEvent;
      }
    }
  }

}
