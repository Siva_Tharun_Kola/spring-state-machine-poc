package com.example.springstatemachine;

import java.util.Arrays;
import java.util.HashSet;

import org.springframework.aop.framework.ProxyFactoryBean;
import org.springframework.aop.target.CommonsPool2TargetSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.statemachine.StateMachine;
import org.springframework.statemachine.config.StateMachineBuilder;

import com.example.springstatemachine.constants.StateMachineConstants;

@EnableConfigurationProperties(TaskStateMachineProps.class)
@Configuration
public class TaskStateMachineConfig {

  @Autowired
  private TaskStateMachineListener taskStateMachineListener;

  @Autowired
  private TaskStateMachineProps taskStateMachineProps;

  @Bean
  @Scope(value = "request", proxyMode = ScopedProxyMode.TARGET_CLASS)
  public ProxyFactoryBean stateMachine() throws Exception {
    ProxyFactoryBean pfb = new ProxyFactoryBean();
    pfb.setTargetSource(poolTargetSource());
    return pfb;
  }

  @Bean
  public CommonsPool2TargetSource poolTargetSource() throws Exception {
    CommonsPool2TargetSource pool = new CommonsPool2TargetSource();
    pool.setMaxSize(taskStateMachineProps.getStateMachinePoolSize());
    pool.setTargetBeanName("stateMachineTarget");
    return pool;
  }

  @Bean("stateMachineTarget")
  @Scope(scopeName = "prototype")
  public StateMachine<String, String> stateMachineTarget() throws Exception {
    StateMachineBuilder.Builder<String, String> builder = StateMachineBuilder.<String, String>builder();
    builder.configureConfiguration().withConfiguration().autoStartup(true);
    builder.configureStates()
        .withStates()
        .initial(StateMachineConstants.States.WAKE_UP)
        .end(StateMachineConstants.States.SLEEP)
        .states(new HashSet<String>(Arrays.asList(StateMachineConstants.States.EAT, StateMachineConstants.States.PLAY,
            StateMachineConstants.States.SLEEP, StateMachineConstants.States.WAKE_UP,
            StateMachineConstants.States.WORK)));
    builder.configureTransitions()
        .withExternal()
        .source(StateMachineConstants.States.SLEEP)
        .target(StateMachineConstants.States.WAKE_UP)
        .event(StateMachineConstants.StateEvents.ALARM_EVENT)
        .and()
        .withExternal()
        .source(StateMachineConstants.States.SLEEP)
        .target(StateMachineConstants.States.WORK)
        .event(StateMachineConstants.StateEvents.POWER_NAP_EVENT)
        .and()
        .withExternal()
        .source(StateMachineConstants.States.EAT)
        .target(StateMachineConstants.States.SLEEP)
        .event(StateMachineConstants.StateEvents.FATIGUE_EVENT)
        .and()
        .withExternal()
        .source(StateMachineConstants.States.EAT)
        .target(StateMachineConstants.States.WORK)
        .event(StateMachineConstants.StateEvents.RESUME_WORK_EVENT)
        .and()
        .withExternal()
        .source(StateMachineConstants.States.PLAY)
        .target(StateMachineConstants.States.SLEEP)
        .event(StateMachineConstants.StateEvents.SLEEP_EVENT)
        .and()
        .withExternal()
        .source(StateMachineConstants.States.PLAY)
        .target(StateMachineConstants.States.WORK)
        .event(StateMachineConstants.StateEvents.RESUME_WORK_EVENT)
        .and()
        .withExternal()
        .source(StateMachineConstants.States.WORK)
        .target(StateMachineConstants.States.PLAY)
        .event(StateMachineConstants.StateEvents.RESUME_WORK_EVENT)
        .and()
        .withExternal()
        .source(StateMachineConstants.States.WORK)
        .target(StateMachineConstants.States.SLEEP)
        .event(StateMachineConstants.StateEvents.FATIGUE_EVENT)
        .and()
        .withExternal()
        .source(StateMachineConstants.States.WAKE_UP)
        .target(StateMachineConstants.States.WORK)
        .event(StateMachineConstants.StateEvents.RESUME_WORK_EVENT);
    builder.configureConfiguration().withConfiguration().listener(taskStateMachineListener);
    return builder.build();
  }

}
