package com.example.springstatemachine;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.statemachine.StateMachine;

@Configuration
public class StateMachineInterceptorConfig {

  private final StateMachine<String, String> stateMachine;
  private final TaskStateMachineInterceptor  taskStateMachineInterceptor;

  @Autowired
  public StateMachineInterceptorConfig(StateMachine<String, String> stateMachine, TaskStateMachineInterceptor taskStateMachineInterceptor) {
    this.stateMachine = stateMachine;
    this.taskStateMachineInterceptor = taskStateMachineInterceptor;
    this.stateMachine.getStateMachineAccessor()
        .withRegion()
        .addStateMachineInterceptor(this.taskStateMachineInterceptor);
  }
}
