package com.example.springstatemachine.constants;

public interface StateMachineConstants {
  interface States {
    String SLEEP   = "SLEEP";
    String WAKE_UP = "WAKE_UP";
    String WORK    = "WORK";
    String PLAY    = "PLAY";
    String EAT     = "EAT";
  }

  interface StateEvents {
    String ALARM_EVENT       = "ALARM_EVENT";
    String POWER_NAP_EVENT   = "POWER_NAP_EVENT";
    String FATIGUE_EVENT     = "FATIGUE_EVENT";
    String RESUME_WORK_EVENT = "RESUME_WORK_EVENT";
    String SLEEP_EVENT       = "SLEEP_EVENT";
  }
}
