package com.example.springstatemachine;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.messaging.Message;
import org.springframework.statemachine.StateContext;
import org.springframework.statemachine.StateMachine;
import org.springframework.statemachine.state.State;
import org.springframework.statemachine.support.StateMachineInterceptor;
import org.springframework.statemachine.transition.Transition;
import org.springframework.stereotype.Component;

@Component
public class TaskStateMachineInterceptor implements StateMachineInterceptor<String, String> {

  Logger logger = LoggerFactory.getLogger(TaskStateMachineInterceptor.class);

  @Override
  public Message<String> preEvent(Message<String> message, StateMachine<String, String> stateMachine) {
    return message;
  }

  @Override
  public void preStateChange(State<String, String> state, Message<String> message, Transition<String, String> transition, StateMachine<String, String> stateMachine) {
    logger.debug(
        "The current transistion is " + transition.getSource().getId() + "to" + transition.getTarget().getId());
  }

  @Override
  public void postStateChange(State<String, String> state, Message<String> message, Transition<String, String> transition, StateMachine<String, String> stateMachine) {
    logger.debug(
        "The transistion is successfully done from " + transition.getSource().getId() + "to" + transition.getTarget()
            .getId());
  }

  @Override
  public StateContext<String, String> preTransition(StateContext<String, String> stateContext) {
    logger.debug("The state Machine is being transitioned from " + stateContext.getStateMachine().getState());
    return stateContext;
  }

  @Override
  public StateContext<String, String> postTransition(StateContext<String, String> stateContext) {
    logger.debug("The state Machine is transitioned to " + stateContext.getStateMachine().getState());
    return stateContext;
  }

  @Override
  public Exception stateMachineError(StateMachine<String, String> stateMachine, Exception exception) {
    return exception;
  }
}
