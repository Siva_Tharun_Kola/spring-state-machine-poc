package com.example.springstatemachine.service;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.statemachine.StateMachine;
import org.springframework.statemachine.access.StateMachineAccess;
import org.springframework.statemachine.support.DefaultStateMachineContext;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.example.springstatemachine.TaskStateMachineProps;

@EnableConfigurationProperties(TaskStateMachineProps.class)
@Service
public class TaskStateMachineServiceImpl implements TaskStateMachineService {

  private final Map<String, Map<String, String>> taskFlowStates;
  private       StateMachine<String, String>     taskStateMachine;

  Logger logger = LoggerFactory.getLogger(TaskStateMachineServiceImpl.class);

  @Autowired
  public TaskStateMachineServiceImpl(TaskStateMachineProps taskStateMachineProps, StateMachine<String, String> taskStateMachine) {
    taskFlowStates = taskStateMachineProps.getDailyTaskStates()
        .stream()
        .collect(Collectors.toMap(TaskStateMachineProps.TaskStatutes::getState, x -> x.getStateEvents()
            .stream()
            .collect(Collectors.toMap(TaskStateMachineProps.TaskStatutes.TaskStateEvents::getNextState,
                TaskStateMachineProps.TaskStatutes.TaskStateEvents::getTriggerEvent))));
  }

  @Override
  public void validateStateMachine(String currentStatus, String newStatus) throws Exception {
    Map<String, String> eventStateTransitionMap = null;
    if (!StringUtils.equals(currentStatus, newStatus)) {
      eventStateTransitionMap = taskFlowStates.get(currentStatus);
    }
    if (CollectionUtils.isEmpty(eventStateTransitionMap)) {
      logger.error("The new status -{}, passed is invalid", newStatus);
      throw new RuntimeException("Invalid new status");
    }
    String event = eventStateTransitionMap.get(newStatus);
    if (StringUtils.isBlank(event)) {
      logger.error("The newStatus - {} that has been passed is invalid", newStatus);
      throw new RuntimeException("Invalid new status");
    }
    resetStateMachine(currentStatus);
  }

  private void resetStateMachine(String currentStatus) {
    taskStateMachine.stop();
    List<StateMachineAccess<String, String>> stateMachineRegions =
        taskStateMachine.getStateMachineAccessor().withAllRegions();
    stateMachineRegions.stream()
        .forEach(access -> access.resetStateMachine(new DefaultStateMachineContext<>(currentStatus, null, null, null)));
    taskStateMachine.start();
  }

}
