package com.example.springstatemachine.service;

public interface TaskStateMachineService {

  void validateStateMachine(String currentStatus,String newStatus) throws Exception;
}
