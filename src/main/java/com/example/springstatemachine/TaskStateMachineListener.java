package com.example.springstatemachine;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.messaging.Message;
import org.springframework.statemachine.StateMachine;
import org.springframework.statemachine.listener.StateMachineListenerAdapter;
import org.springframework.statemachine.state.State;
import org.springframework.stereotype.Component;

@Component
public class TaskStateMachineListener extends StateMachineListenerAdapter<String,String> {

  Logger logger = LoggerFactory.getLogger(TaskStateMachineListener.class);

  @Override
  public void stateMachineStarted(StateMachine<String, String> stateMachine) {
    logger.debug("stateMachine has started with initial state : " + stateMachine.getState());
  }

  @Override
  public void stateMachineStopped(StateMachine<String, String> stateMachine) {
    logger.debug("stateMachine has stopped with final state : " + stateMachine.getState());
  }

  @Override
  public void stateChanged(State<String, String> from, State<String, String> to) {
    logger.debug("program state changed to " + to.getId());
  }

  @Override
  public void eventNotAccepted(Message<String> event) {
    logger.error("The Event " + event.getPayload() + " is a invalid event");
  }
}
