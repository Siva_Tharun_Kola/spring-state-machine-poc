package com.example.springstatemachine;

import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.statemachine.StateMachine;
import org.springframework.statemachine.access.StateMachineAccess;
import org.springframework.statemachine.support.DefaultStateMachineContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import com.example.springstatemachine.constants.StateMachineConstants;
import com.example.springstatemachine.service.TaskStateMachineServiceImpl;

@RunWith(SpringRunner.class)
@ActiveProfiles("test")
@SpringBootTest(classes = SpringStateMachineApplicationTests.class)
public class TaskStateMachineTest {

  @Rule
  public ExpectedException thrown = ExpectedException.none();

  @Autowired
  private StateMachine<String,String> taskStateMachine;
  @Autowired
  private TaskStateMachineServiceImpl taskStateMachineServiceImpl;

  @Before
  public void testStartStateMachine() throws Exception {
    taskStateMachine.start();
  }

  @After
  public void testStopStateMachine() throws Exception {
    taskStateMachine.stop();
  }

  public void resetStateMachine(String newState) {
    taskStateMachine.stop();
    List<StateMachineAccess<String,String>> stateMachineRegions = taskStateMachine.getStateMachineAccessor().withAllRegions();
    stateMachineRegions.stream().forEach(access -> access.resetStateMachine(new DefaultStateMachineContext<>(newState, null, null, null)));
    taskStateMachine.start();
  }

  @Test
  public void testSleepToWakeUpStateTransition() throws Exception{
    taskStateMachineServiceImpl.validateStateMachine(StateMachineConstants.States.SLEEP,StateMachineConstants.States.WAKE_UP);
    resetStateMachine(StateMachineConstants.States.WAKE_UP);
  }

  @Test
  public void testSleepToWorkStateTransition() throws Exception {
    taskStateMachineServiceImpl.validateStateMachine(StateMachineConstants.States.SLEEP,StateMachineConstants.States.WORK);
    resetStateMachine(StateMachineConstants.States.WORK);
  }

}
