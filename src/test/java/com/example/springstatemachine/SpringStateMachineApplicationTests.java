package com.example.springstatemachine;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.statemachine.StateMachine;
import org.springframework.statemachine.test.StateMachineTestPlan;
import org.springframework.statemachine.test.StateMachineTestPlanBuilder;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.example.springstatemachine.constants.StateMachineConstants;

@ActiveProfiles("test")
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = {TaskStateMachineTest.class})
class SpringStateMachineApplicationTests {

  @Autowired
  private TaskStateMachineConfig taskStateMachineConfig;

  private StateMachine<String, String> taskStateMachine;

  @Before
  public void setup() throws Exception {
    taskStateMachine = taskStateMachineConfig.stateMachineTarget();
  }

  @Test
  public void testInitialState() throws Exception {
    StateMachineTestPlan<String, String> plan =
        StateMachineTestPlanBuilder.<String, String>builder().stateMachine(taskStateMachine)
            .step()
            .expectState(StateMachineConstants.States.WAKE_UP)
            .expectStateMachineStarted(1)
            .and()
            .build();
    plan.test();
  }

  @Test
  public void testIncorrectEventForStateTransition() throws Exception {
    StateMachineTestPlan<String, String> plan =
        StateMachineTestPlanBuilder.<String, String>builder().stateMachine(taskStateMachine)
            .step()
            .expectState(StateMachineConstants.States.PLAY)
            .and()
            .step()
            .sendEvent(MessageBuilder.withPayload(StateMachineConstants.StateEvents.FATIGUE_EVENT).build())
            .expectStates(StateMachineConstants.States.WORK)
            .expectTransition(0)
            .expectStateMachineStopped(0)
            .and()
            .build();
    plan.test();
  }

  @Test
  public void testPlayStateToSleep() throws Exception {
    StateMachineTestPlan<String, String> plan =
        StateMachineTestPlanBuilder.<String, String>builder().stateMachine(taskStateMachine)
            .step()
            .expectState(StateMachineConstants.States.PLAY)
            .and()
            .step()
            .sendEvent(MessageBuilder.withPayload(StateMachineConstants.StateEvents.RESUME_WORK_EVENT).build())
            .expectStates(StateMachineConstants.States.WORK)
            .expectTransition(1)
            .and()
            .step()
            .sendEvent(MessageBuilder.withPayload(StateMachineConstants.StateEvents.FATIGUE_EVENT).build())
            .expectStates(StateMachineConstants.States.SLEEP)
            .expectTransition(1)
            .and()
            .build();
  }

}
