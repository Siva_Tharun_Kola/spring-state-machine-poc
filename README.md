# Spring State Machine POC

This repository is a sample project that implements a finite state machine using a spring state machine (https://projects.spring.io/spring-statemachine/).
Include the unit test cases in Junit